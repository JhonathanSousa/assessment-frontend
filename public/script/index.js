const endpoints = {
  category: 'http://localhost:8888/api/V1/categories/',
  list: 'http://localhost:8888/api/V1/categories/list',
};

// Criando função para criar menu
async function createMenu() {
  const menuContainers = document.querySelectorAll('.category-list');
  let list;

  try {
    const response = await fetch(endpoints.list, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    list = await response.json();
    list = list.items;
  } catch (error) {
    console.log(error);
  }

  const items = list?.map(item => {
    return `
    <li><a href="?categoryId=${item.id}&categoryName=${encodeURIComponent(item.name)}">${item.name}</a></li>`
  }).join('');
  menuContainers.forEach(container => {
    container.innerHTML = items;
  });

}

// Criando função para listar produtos na tela
async function getProducts(){
  const container = document.getElementById("produtos");
  const categoryTitle = document.querySelectorAll('.category-name');
  const queryParams = document.location.href.split('?')[1];
  const urlId = Number(queryParams.split('&')[0].split('=')[1]);
  const categoryName = decodeURIComponent(queryParams.split('&')[1].split('=').pop());
  const categoryId = urlId ? urlId : "1";

  let products;
  let filters;

  try {
    const response = await fetch(endpoints.category + categoryId, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });

    products = await response.json();
    filters = products.filters;
    products = products.items;

  }catch(error){
    console.log(error);
  }

  const items = products?.map(product => {
    return `
        <li class="produtos__item">
          <figure class="produtos__image">
            <img src="${product.image}" alt="Peça sua roupa"/>
          </figure>
          <h2 class="produtos__titulo">${product.name}</h2>
          <p class="produtos__preco">R$ ${product.price}</p>
          <button class="produtos__button">comprar</button>
        </li>
    `;
  }).join('');

  container.innerHTML = items;
  
  categoryTitle.forEach(title => {
    title.innerHTML = categoryName;
  });

}

createMenu();
getProducts();